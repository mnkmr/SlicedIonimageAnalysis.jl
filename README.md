# SlicedIonimageAnalysis

[![Build Status](https://gitlab.com/mnkmr/SlicedIonimageAnalysis.jl/badges/master/build.svg)](https://gitlab.com/mnkmr/SlicedIonimageAnalysis.jl/pipelines)
[![Coverage](https://gitlab.com/mnkmr/SlicedIonimageAnalysis.jl/badges/master/coverage.svg)](https://gitlab.com/mnkmr/SlicedIonimageAnalysis.jl/commits/master)

This package provides functions to analyze sliced photofragment ion imaging images.


## Install

Press `]` to start [Pkg REPL mode](https://docs.julialang.org/en/v1/stdlib/Pkg/), then execute:

```
(v1.1) pkg> registry add https://gitlab.com/mnkmr/MNkmrRegistry.git
(v1.1) pkg> add SlicedIonimageAnalysis
```


## Usage

Load an image as a `Matrix{<:Real}` (`Array{<:Real,2}`) and pass it to `SlicedImage` constructor. The radial, speed, energy, and angular distributions can be obtained through the `radialdist`, `speeddist`, `energydist`, and `anisotropy` methods.

```julia
using SlicedIonimageAnalysis
using PyPlot

# visualize the original image
imshow(img);

# construct a SlicedImage with a certain center
s = SlicedImage(img, (201, 201))

# convert a SlicedImage into an Array{Float64,2} (Matrix{Float64})
img = Array(s);

# plot the radial distribution
r, Ir = radialdist(s);
clf();
plot(r, Ir);

# plot the speed distribution
ppm = 10e3   # pixel-per-meter (px/m)
tof = 10e-6  # time-of-flight in second
N   = 1.5    # magnification factor
v, Iv = speeddist(s, ppm, tof, N);
clf();
plot(v, Iv);

# calculate the speed distribution from the radial distribution
v, Iv = speeddist(r, Ir, ppm, tof, N);
clf();
plot(v, Iv);

# plot the energy distribution (For example, observing I⁺ of CH₃I -> CH₃ + I)
m = 127      # the mass of photofragment
M = 142      # the mass of parent molecule
Et, IEt = energydist(s, ppm, tof, N, m, M);
clf();
plot(Et, IEt);

# calculate the energy distribution from the speed distribution
Et, IEt = energydist(v, Iv, m, M);
clf();
plot(Et, IEt);

# plot β₂ along the radial axis
# NOTE that the  β₂ may not be reasonable if the corresponding radial distribution is too weak
r, b = anisotropy(s);
β = b[:, 1];
clf();
plot(r, β, "o");
```
