module SlicedIonimageAnalysis

export
    SlicedImage,
    crop,
    shear,
    average,
    smooth,
    radialdist,
    speeddist,
    energydist,
    anisotropy,
    angulardist

using IonimageAnalysisCore:
    IonimageAnalysisCore,
    maxradius,
    legendre,
    crop,
    shear,
    average,
    smooth,
    angularfit,
    radialdist,
    speeddist,
    energydist,
    anisotropy
using LinearAlgebra: Diagonal, pinv
using Dierckx: Spline2D
using Printf: @printf


struct SlicedImage{T<:Real} <: AbstractMatrix{T}
    arr::Matrix{T}
    center::NTuple{2,Float64}
    fold::Int
    r::Vector{Float64}

    function SlicedImage(arr::AbstractMatrix{T}, center::NTuple{2,<:Real}, fold::Integer, r::Vector{<:Real}) where {T<:Real}
        new{T}(arr, center, fold, r)
    end
end

"""
    SlicedImage(image, center; rmax=nothing, fold=2)

Return a slice image data containing radial and angular distributions.
Those data can be obtained through the interface functions.
See also: [`radialdist`](@ref), [`speeddist`](@ref), [`energydist`](@ref),
          [`anisotropy`](@ref), [`angulardist`](@ref)

# Arguments
- `image` : the matrix of image
- `center`: the (symmetric) center coordinate of `image` for analysis.
- `rmax`  : the maximum radius to obtain the radial distribution.
- `fold`  : the number of folding symmetry of the image, usually 2
            to obtain the angular distribution in 0 - 180 degree.
            probably, only 1, 2, and 4 make sense.

# Examples
```julia
julia> using SlicedIonimageAnalysis

julia> s = SlicedImage(image, (100, 100))
```
"""
function SlicedImage(image::AbstractMatrix{<:Real}, center; rmax=nothing, fold::Integer=2)
    if isnothing(rmax)
        rmax = maxradius(image, center)
    end
    center = (center[1], center[2])  # into tuple if it is an array
    !isinside(center, image) && ArgumentError("The center coordinate $(center) is outside of the $(size(image, 1))×$(size(image, 2)) image.") |> throw
    rmax = min(rmax, maxradius(image, center))
    r = collect(1.0:1.0:rmax)
    SlicedImage(image, center, fold, r)
end

function Base.show(io::IO, ::MIME"text/plain", s::SlicedImage)
    h, w = size(s.arr)
    @printf("  size     : %d × %d\n", h, w)
    @printf("  center   : %s\n", s.center)
    @printf("  intensity: %6.2e", sum(s.arr))
end

Base.Matrix(s::SlicedImage) = copy(s.arr)
Base.Array(s::SlicedImage) = copy(s.arr)
Base.convert(::Type{T}, s::SlicedImage) where T<:Matrix = copy(s.arr)
Base.convert(::Type{AbstractMatrix{Float64}}, s::SlicedImage) = copy(s.arr)

# AbstractArray interfaces
Base.size(s::SlicedImage) = size(s.arr)
Base.getindex(s::SlicedImage, I...) = getindex(s.arr, I...)
Base.length(s::SlicedImage) = length(s.arr)
Base.axes(s::SlicedImage) = map(Base.OneTo, size(s.arr))
Base.strides(s::SlicedImage) = strides(s.arr)
Base.stride(s::SlicedImage, k::Integer) = stride(s.arr, k)


"""
    r, I = radialdist(s::SlicedImage)

Calculate a radial distribution from a sliced image data.
See also: [`SlicedImage`](@ref)
"""
function IonimageAnalysisCore.radialdist(s::SlicedImage)
    image = s.arr
    center = s.center
    spline = interpolator(image)
    r = copy(s.r)
    Ir = Vector{Float64}(undef, length(r))
    for (i, r) in enumerate(r)
        Ir[i] = _radialdist(spline, center, r)
    end
    r, Ir
end

function _radialdist(spline, center, r)
    # expects approximately 2π*r pixels data are on an arc of the raw image
    sample = round(Int, 2π*r, RoundNearestTiesUp)
    Ir = 0.0
    for θ in LinRange(0.0, 360.0, sample)
        y, x = cartpos(center, θ, r)
        Ir += spline(y, x)*r*abs(sind(θ))
    end
    Ir
end


"""
    v, I = speeddist(s::SlicedImage, ppm, tof, N)

Calculate a speed distribution from a sliced image data.
See also: [`SlicedImage`](@ref)
"""
function IonimageAnalysisCore.speeddist(s::SlicedImage, ppm, tof, N)
    r, I = radialdist(s)
    speeddist(r, I, ppm, tof, N)
end


"""
    Et, IEt = energydist(s::SlicedImage, ppm, tof, N, m, M)

Calculate a energy distribution from a sliced image data.
See also: [`SlicedImage`](@ref)
"""
function IonimageAnalysisCore.energydist(s::SlicedImage, ppm, tof, N, m, M)
    v, I = speeddist(s, ppm, tof, N)
    energydist(v, I, m, M)
end


"""
    r, b, σb, l = anisotropy(s::SlicedImage; l=[2])

Compute the anisotropy parameters from a sliced image data.
See also: [`SlicedImage`](@ref)
"""
function IonimageAnalysisCore.anisotropy(s::SlicedImage; l=[2])
    spline = interpolator(s.arr)
    l = filter(x -> x > 0, sort(unique(l)))
    β    = zeros(Float64, (length(s.r), length(l)))
    βerr = zeros(Float64, (length(s.r), length(l)))
    for (i, r) in enumerate(s.r)
        θ, I = _angulardist(spline, s.center, r, 0, s.fold)
        b, berr = _anisotropy(θ, I, l)
        β[i, :] .= b
        βerr[i, :] .= berr
    end
    copy(s.r), β, βerr, l
end

function _anisotropy(θ, I, l)
    # Not enough data points for fitting
    length(I) <= length(l) && return nans(length(l)), nans(length(l)), NaN
    return leastsquaresfit(θ, I, l)
end

function interpolator(image)
    ymax, xmax = size(image)
    y = 1.0:float(ymax)
    x = 1.0:float(xmax)
    Spline2D(y, x, image; ky=1, kx=1)
end

function leastsquaresfit(θ, I, l)
    A = Matrix{Float64}(undef, length(θ), length(l) + 1)
    A[:, 1] .= 1.0
    for (i, l) in enumerate(l), (j, cosθ) in enumerate(cosd.(θ))
        A[j, i+1] = legendre(l, cosθ)
    end
    varI = variance(I)
    w    = Diagonal(@.(1 / sqrt(varI)))
    A⁺   = pinv(w*A)
    A′   = A⁺*w
    B    = A′*I
    varB = propagate_variance(varI, A′)
    if B[1] == 0
        return nans(length(l)), nans(length(l)), B[1]
    end
    b = @. B[2:end]/B[1]
    berr = @. sqrt((1/B[1])^2*varB[2:end] + (b/B[1]^2)^2*varB[1])
    return b, berr, B[1]
end


"""
    θ, Iθ, Ifit, b, σb, l = angulardist(θ, Iθ; l=[2], norm=true)

Return the angular fitting curve of an angular distribution.
See also: [`angularfit`](@ref), [`anisotropy`](@ref)

# Outputs
- `θ`   : the vector of angles.
- `Iθ`  : the vector of angular distribution of the image at a radius.
- `Ifit`: the best-fit curve of `Iθ`.
- `b`   : the matrix of fitting parameters.
- `σb`  : the matrix of standard error of `b`.
- `l`   : the vector of orders of Legendre polynomials to fit angular distributions.

# Arguments
- `θ`   : the vector of angles.
- `Iθ`  : the vector of angular distribution of the image at a radius.
- `l`   : See below.
- `norm`: normalizing `Iθ` if true; `Iθ./b[end]`.

# Argument `l`
`l` is a vector of orders of Legendre polynomials to take into account for
angular fitting. The items in `l` should be integer larger than zero (1, 2, ...)
If `l = [2]`, the angular distribution is fitted by
    I(θ) = B(1 + β₂*P₂(cosθ)).
The resulted `b` is a `length(r)`×`length(l)` matrix; therefore, `b[:, 1]` is
the vector of `β₂`. If `l` includes additional terms, the columns of `b` will
be increased. In case `l = [2, 4]`, the angular distribution is fitted by
    I(θ) = B(1 + β₂*P₂(cosθ) + β₄*P₄(cosθ)).
Then the obtained `b` would be `[β₂, β₄]`. The corresponding order number is
shown in the returned `l`. `B` is the intensity factor and thus it is
proportional to the radialdistribution `Ir`. See [`radialdist`](@ref).

# Example
```julia
julia> using SlicedIonimageAnalysis, PyPlot

julia> θ, Iθ, Ifit = angulardist(0:1:180, image, 100); # obtain the angular distribution at the 100th radius

julia> plot(θ, Iθ, "o")

julia> plot(θ, Ifit, "-")
```
"""
function angulardist(θ, Iθ; l=[2], norm=true)
    θ = copy(θ)
    Iθ = copy(Iθ)
    l = filter(x -> x > 0, sort(unique(l)))
    β, βerr, B = _anisotropy(θ, Iθ, l)
    if norm && B != 0
        Iθ ./= B
    end
    Ifit = angularfit(θ, β, l)
    if !norm
        Ifit .*= B
    end
    return θ, Iθ, Ifit, β, βerr, l
end

"""
    θ, Iθ, Ifit, b, σb, l = angulardist(s::SlicedImage, r::Real; num::Integer=0, kwargs...)

Return the angular distribution at the radius `r` of the sliced image data.
`num` is the number of data points, that is `length(θ) == length(Iθ) == num`.
If `num` is less than 1, `floor(Int, 2π*r/fold)` will be used.
See also: [`SlicedImage`](@ref)
"""
function angulardist(s::SlicedImage, r::Real; num::Integer=0, kwargs...)
    r <= 0 && throw(ArgumentError("`r` should be positive."))
    spline = interpolator(s.arr)
    θ, Iθ = _angulardist(spline, s.center, r, num, s.fold)
    angulardist(θ, Iθ; kwargs...)
end


function _angulardist(spline, center, r, num, fold)
    if num < 1
        # Expecting that approximately 2π*r pixels are on an arc.
        # To avoid over-sampling, sample data every 4 px on an arc.
        # The number 9 is the minimum number to fit with including
        # up to four-th order Legendre polynomials.
        num = max(9, floor(Int, π*r/2/fold))
    end
    θarrays = splitcircle(fold, num)
    I = zeros(Float64, size(θarrays, 1))
    for n in 1:fold
        θiter = θarrays[:, n]
        for (j, θ) in enumerate(θiter)
            y, x = cartpos(center, θ, r)
            I[j] += spline(y, x)
        end
    end
    I ./= fold
    θarrays[:, 1], I
end


"Split a circle (0 to 360 degree) into symmetric blocks"
function splitcircle(fold::Integer, num)
    fold ≤ 0 && throw(ArgumentError("`fold` should be a positive integer"))
    θchunk = 360.0/fold
    θarrays = zeros(Float64, (num, fold))
    for i in 1:fold
        θmin = (i-1)*θchunk
        θmax = i*θchunk
        θiter = LinRange(θmin, θmax, num)
        if iseven(i)
            θiter = reverse(θiter)
        end
        for (j, θ) in enumerate(θiter)
            θarrays[j, i] = θ
        end
    end
    θarrays
end


"Calculate weighting factor for fitting assuming that the signal obeys Poisson statistics"
function variance(I)
    all(I .≈ 0) && return ones(eltype(I), length(I))

    # Assuming that the signal counts obey Poisson statistics. If the count is
    # big, practically N > 10, it can be approximated to A normal distribution
    # with the standard deviation σI = √I, where I is the count of the pixel.
    # The weight for a residual is the inverse of the square of σI; w = 1/σI².
    σI² = abs.(I)

    # Consider that any pixel has a non-zero standard deviation σIₘᵢₙ.
    # It is estimated to the square root of the non-zero minimum of σI².
    # This is to avoid to obtain w == Inf.
    σImin² = minimum(filter(x -> x > 0, σI²))
    σI²[σI² .== 0] .= σImin²
    return σI²
end


"""
    propagate_variance(varI::Vector, G⁺)

Return the diagonal terms of the variance-covariance matrix of coefficient C
matrix. The variances of coefficient matrix C can be obtained as the diagonal
terms of its covariance matrix.
    varC = diag(G⁺*varI*transpose(G⁺))
"""
function propagate_variance(varI::Vector, G⁺)
    var = zeros(Float64, (size(G⁺, 1),))
    for j in 1:size(G⁺, 2), i in 1:size(G⁺, 1)
        var[i] += G⁺[i, j]^2*varI[j]
    end
    var
end


"Return an array all filled with Nan"
function nans(shape...)
    nanmatrix = Array{Float64,length(shape)}(undef, shape...)
    nanmatrix .= NaN
    nanmatrix
end


"Return true if the coordinate is inside the image"
isinside(c, img) = 1 < c[1] < size(img, 1) && 1 < c[2] < size(img, 2)


"Calculate a Cartesian position (y, x) from a polar position (θ, r)"
cartpos(center, θ, r) = cartpos(center[1], center[2], θ, r)
cartpos(yc, xc, θ, r) = yc - r*cosd(θ), xc + r*sind(θ)


end # module
