using DelimitedFiles: readdlm
using SlicedIonimageAnalysis
using Test

@testset "Unit Test" begin
    @test all(SlicedIonimageAnalysis.cartpos((3, 3), 0.,   2) .≈ (1., 3.))
    @test all(SlicedIonimageAnalysis.cartpos((3, 3), 90.,  2) .≈ (3., 5.))
    @test all(SlicedIonimageAnalysis.cartpos((3, 3), 180., 2) .≈ (5., 3.))
    @test all(SlicedIonimageAnalysis.cartpos((3, 3), 270., 2) .≈ (3., 1.))
    @test all(SlicedIonimageAnalysis.cartpos(3, 3, 0.,   2) .≈ (1., 3.))
    @test all(SlicedIonimageAnalysis.cartpos(3, 3, 90.,  2) .≈ (3., 5.))
    @test all(SlicedIonimageAnalysis.cartpos(3, 3, 180., 2) .≈ (5., 3.))
    @test all(SlicedIonimageAnalysis.cartpos(3, 3, 270., 2) .≈ (3., 1.))

    @test SlicedIonimageAnalysis.isinside((6, 6), zeros(Float64, (11, 11))) == true
    @test SlicedIonimageAnalysis.isinside((2, 2), zeros(Float64, (11, 11))) == true
    @test SlicedIonimageAnalysis.isinside((2, 6), zeros(Float64, (11, 11))) == true
    @test SlicedIonimageAnalysis.isinside((6, 2), zeros(Float64, (11, 11))) == true
    @test SlicedIonimageAnalysis.isinside((1, 1), zeros(Float64, (11, 11))) == false
    @test SlicedIonimageAnalysis.isinside((1, 6), zeros(Float64, (11, 11))) == false
    @test SlicedIonimageAnalysis.isinside((6, 1), zeros(Float64, (11, 11))) == false
    @test SlicedIonimageAnalysis.isinside((0, 0), zeros(Float64, (11, 11))) == false
    @test SlicedIonimageAnalysis.isinside((0, 6), zeros(Float64, (11, 11))) == false
    @test SlicedIonimageAnalysis.isinside((6, 0), zeros(Float64, (11, 11))) == false
end

function test_SlicedImage(s)
    @test s isa SlicedIonimageAnalysis.SlicedImage
    @test size(s) == size(s.arr)
    @test getindex(s, :) == getindex(s.arr, :)
    @test length(s) == length(s.arr)
    @test axes(s) == axes(s.arr)
    @test strides(s) == strides(s.arr)
    @test stride(s, 1) == stride(s.arr, 1)
    @test stride(s, 2) == stride(s.arr, 2)

    r, Ir = radialdist(s)
    @test all(0.0 .<= r .<= 70.01)
    @test length(r) == length(Ir) == 70
    @test maximum(Ir[1:33]) ≤ 0
    @test 4 ≤ argmax(Ir[36:45]) ≤ 6
    @test 4 ≤ argmax(Ir[46:55]) ≤ 6
    @test 4 ≤ argmax(Ir[56:65]) ≤ 6

    v, Iv = speeddist(s, 10e3, 1e-6, 2.0)
    @test all(0.0 .<= v .<=  3500.1)
    @test length(v) == length(Iv) == 70
    @test maximum(Iv[1:33]) ≤ 0
    @test 4 ≤ argmax(Ir[36:45]) ≤ 6
    @test 4 ≤ argmax(Ir[46:55]) ≤ 6
    @test 4 ≤ argmax(Ir[56:65]) ≤ 6

    Et, IEt = energydist(s, 10e3, 1e-6, 2.0, 15, 50)
    @test all(0.0 .<= Et .<=  131.251)
    @test length(Et) == length(IEt) == 70
    @test maximum(IEt[1:33]) ≤ 0
    @test 4 ≤ argmax(Ir[36:45]) ≤ 6
    @test 4 ≤ argmax(Ir[46:55]) ≤ 6
    @test 4 ≤ argmax(Ir[56:65]) ≤ 6

    diff(arr, expect) = abs(sum(arr)/length(arr) - expect)
    r, b, berr, l = anisotropy(s)
    β2 = b[:, 1]
    β2err = berr[:, 1]
    @test size(b, 1) == size(berr, 1) == size(r, 1)
    @test size(b, 2) == size(berr, 2) == size(l, 1)
    @test all(isnan.(β2[1:28]))
    @test all(isnan.(β2err[1:28]))
    @test diff(β2[39:41], -1) < 0.01
    @test diff(β2[49:51],  0) < 0.01
    @test diff(β2[59:61],  2) < 0.01
    @test β2err[40] < 0.1
    @test β2err[50] < 0.1
    @test β2err[60] < 0.1
end

@testset "SlicedIonimageAnalysis.jl" begin
    let img = readdlm(joinpath(@__DIR__, "test01.tsv"))
        s = SlicedImage(img, (71, 71))
        test_SlicedImage(s)
    end

    let img = readdlm(joinpath(@__DIR__, "test01.tsv"))
        s = SlicedImage(float.(img), (71, 71))
        test_SlicedImage(s)
    end


    let img = readdlm(joinpath(@__DIR__, "test02.tsv"))
        s = SlicedImage(img, (71.5, 71.5))
        test_SlicedImage(s)
    end

    let img = readdlm(joinpath(@__DIR__, "test02.tsv"))
        s = SlicedImage(float.(img), (71.5, 71.5))
        test_SlicedImage(s)
    end

    let img = readdlm(joinpath(@__DIR__, "test01.tsv"))
        s = SlicedImage(img, (71, 71))
        θ, Iθ = angulardist(s, 60; num=90)
        @test length(θ) == length(Iθ) == 90
    end
end
